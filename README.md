# Git Going!
A tool to run a GitLab CI/CD pipeline job

See the help message for information on the parameters and usage: `./git_going -h`

A use case for this scipt would be if you wanted to schedule a pipeline job to run 
at some point in the future without having to manually trigger it yourself (say at 2 in the morning). 
With this script, you can simply make an `at` job for it with the required parameters:  

```
at 2am
/path/to/repo/git_going -p 100 -j 1000
```

In order to not have to put your [GitLab API](https://docs.gitlab.com/12.10/ee/user/profile/personal_access_tokens.html) 
token in the command, you will need to 
save it to a file called `~/.gitlab_token` with the only contents being your token.
